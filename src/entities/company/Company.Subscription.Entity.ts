import { ObjectId } from 'mongoose';
import { CompanyEntity } from './Company.Entity';

export interface SubscriptionEntity {
    _id: string | ObjectId;
    company_id: CompanyEntity | CompanyEntity['_id'];
    startAt: Date;
    endAt: Date;
}
