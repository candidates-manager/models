import { ObjectId } from 'mongoose';
import { JobResponseModel } from '../job/Job.Entity';

//Entity
export interface CompanyEntity {
    _id: string | ObjectId;
    name: string;
    urls?: CompanyUrlsEntity;
    updatedAt?: Date;
    createdAt?: Date;
}
export interface CompanyUrlsEntity {
    linkedin: string;
    glassdoor: string;
}

//ResponseModel
export interface CompanyResponseModel extends CompanyEntity {
    jobs: JobResponseModel[];
}

//InputModel
export interface CompanyInputModel {
    name: string;
}

//Update
export interface CompanyUpdateModel {}

//ArgsModel
export interface CompanyArgsModel {
    company_id?: [String];
}
