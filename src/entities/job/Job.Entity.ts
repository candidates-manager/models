import { ObjectId } from 'mongoose';
import { CompanyEntity } from '../company/Company.Entity';
import { ApplicationResponseModel } from '../application/Application.Entity';
import { ApplicationProcessEntity } from '../application/Application.Process.Entity';
//Enums
import JobStatusEnum from '../../enums/Job.Status.Enum';

//Entity
export interface JobEntity {
    _id: string | ObjectId;
    company_id: CompanyEntity | ObjectId;
    application_process_id: ApplicationProcessEntity | ObjectId;
    name: string;
    status: JobStatusEnum;
    link?: string;
    updatedAt?: Date;
    createdAt?: Date;
}

//ResponseModel
export interface JobResponseModel extends JobEntity {
    applications?: ApplicationResponseModel[];
}

//InputModel
export interface JobInputModel {
    name: string;
    company_id: string;
    application_process_id: string;
}

//Update
export interface JobUpdateModel {
    application_process_id?: string;
    name?: string;
    status?: JobStatusEnum;
}

//ArgsModel
export interface JobArgsModel {
    company_id: string[];
    status?: JobStatusEnum[];
    job_id?: string[];
    public_id?: string;
}
