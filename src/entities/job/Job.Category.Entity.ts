import { ObjectId } from 'mongoose';
import { JobEntity } from './Job.Entity';
export interface JobCategoryEntity {
    _id: string | ObjectId;
    job_id: JobEntity | JobEntity['_id'];
    parent_id: JobCategoryEntity | JobCategoryEntity['_id'];
    name: string;
    updatedAt?: Date;
    createdAt?: Date;
}

export interface JobCategoryResponseModel extends JobCategoryEntity {}
