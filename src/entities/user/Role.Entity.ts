import { ObjectId } from 'mongoose';
import RoleEnum from '../../enums/Role.Enum';
import { UserResponseModel } from './User.Entity';
export interface RoleEntity {
    _id: string | ObjectId;
    name: RoleEnum;
    public_id: string;
}
export interface RoleResponseModel extends RoleEntity {
    users: UserResponseModel[];
}
export interface RoleInputModel {}
export interface RoleArgsModel {
    public_id?: string[];
}
