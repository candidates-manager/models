import { ObjectId } from 'mongoose';
import { CompanyEntity } from '../company/Company.Entity';
import { CandidateEntity } from '../candidate/Candidate.Entity';
import { RoleEntity } from './Role.Entity';
import { UserStatusEnum } from '@/Enums';
export interface UserEntity {
    _id: string | ObjectId;
    role_id: RoleEntity[] | RoleEntity['_id'][];
    company_id: CompanyEntity[] | CompanyEntity['_id'][];
    candidate_id: CandidateEntity | CandidateEntity['_id'];
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    status: UserStatusEnum;
    function?: string;
    avatar?: string;
    urls?: UserUrlsEntity;
    updatedAt?: Date;
    createdAt?: Date;
}

export interface UserUrlsEntity {
    linkedin: string;
}

export interface UserResponseModel extends UserEntity {}
export interface UserInputModel {
    email: string;
    password: string;
    firstname: string;
    lastname: string;
    avatar: string;
    company_id: string[];
    role_id: string[];
    function?: string;
    linkedin: string;
    status?: UserStatusEnum;
}
export interface UserUpdateModel {
    email?: string;
    firstname?: string;
    lastname?: string;
    avatar?: string;
    company?: string[];
    role?: string[];
    function?: string;
    linkedin: string;
    status?: UserStatusEnum;
}
export interface UserArgsModel {
    _id?: string;
    email?: string;
    role?: string[];
    company?: string[];
}

export {};
