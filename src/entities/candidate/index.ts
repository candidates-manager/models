import * as CandidateEntity from './Candidate.Entity';
import * as CandidateSettings from './Candidate.Settings.Entity';
export { CandidateEntity, CandidateSettings };
