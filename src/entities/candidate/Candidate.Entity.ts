import { ObjectId } from 'mongoose';
import { JobCategoryEntity } from '../job/Job.Category.Entity';

//Entity
export interface CandidateEntity {
    _id: string | ObjectId;
    email: string;
    firstname: string;
    lastname: string;
    access_token?: string;
    job_category?: JobCategoryEntity | JobCategoryEntity['_id'];
    urls?: CandidateUrlsEntity;
    rating?: CandidatenRatingEntity;
    updatedAt?: Date;
    createdAt?: Date;
}

export interface CandidateUrlsEntity {
    linkedin: string;
    resume: string;
    websites: string[];
}

export interface CandidatenRatingEntity {
    experience: Number;
    skills: Number;
    softskills: Number;
}

//ResponseModel
export interface CandidateResponseModel extends CandidateEntity {}

//InputModel
export interface CandidateInputModel {
    email: string;
    access_token: string;
    firstname?: string;
    lastname?: string;
    linkedin?: string;
    rating_experience?: Number;
    rating_skills?: Number;
    rating_softskills?: Number;
}

//Update
export interface CandidateUpdateModel {
    firstname?: string;
    lastname?: string;
    linkedin?: string;
    rating_experience?: Number;
    rating_skills?: Number;
    rating_softskills?: Number;
}

//ArgsModel
export interface CandidateArgsModel {
    company_id: string[];
}
