//Entities
import { UserResponseModel } from '@/Entities';

//Entities
export class AuthPayloadEntity {
    user_id: string;
    companies?: string[];
    roles?: string[];
    candidate?: string;
}

export interface AuthResponseModel {
    token: string;
    credentials: UserResponseModel;
}

//InputModel
export interface AuthInputModel {
    email: string;
    password: string;
}
