import { ObjectId, Types } from 'mongoose';
import { UserEntity } from '../user/User.Entity';
import { ApplicationEntity } from './Application.Entity';
import { ApplicationReviewEntity } from './Application.Review.Entity';
export interface ApplicationStepEntity {
    _id: string | ObjectId;
    application_id: ApplicationEntity | Types.ObjectId;
    step: Number;
    name: String;
    description?: String;
    duration: Number;
    users?: UserEntity[] | ObjectId[];
    reviews?: ApplicationReviewEntity[] | ObjectId[];
    createdAt: Date;
    updatedAt: Date;
}
export interface ApplicationStepResponseModel extends ApplicationStepEntity {}
export interface ApplicationStepInputModel {
    application_id: ObjectId;
    step: Number;
    name: String;
    description?: String;
    duration: Number;
    users?: string[] | Types.ObjectId[];
}
export interface ApplicationStepUpdateModel {
    name?: String;
    description?: String;
    duration?: Number;
    users?: UserEntity[] | ObjectId[];
}
export interface ApplicationStepArgsModel {
    _id: string | ObjectId;
}
