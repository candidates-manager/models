import { ObjectId, Types } from 'mongoose';
import { UserEntity } from '../user/User.Entity';
import { ApplicationEntity } from './Application.Entity';
import { ApplicationStepEntity } from './Application.Step.Entity';

export interface ApplicationReviewEntity {
    _id: string | ObjectId;
    application_id?: ApplicationEntity | ObjectId;
    step_id?: ApplicationStepEntity | ObjectId;
    rating: Number;
    description: String;
    user_id: UserEntity | ObjectId;
    createdAt: Date;
    updatedAt: Date;
}
export interface ApplicationReviewResponseModel
    extends ApplicationReviewEntity {}
export interface ApplicationReviewInputModel {
    application_id: String;
    step_id: String;
    user_id: String;
    rating: Number;
    description: String;
}
export interface ApplicationReviewUpdateModel {
    rating: Number;
    description: String;
}
export interface ApplicationReviewArgsModel {
    step_id?: string[];
    application_id?: string[];
}
