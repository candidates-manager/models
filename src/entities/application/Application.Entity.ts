import { ObjectId } from 'mongoose';

//Entities
import { JobResponseModel } from '../job/Job.Entity';
import { UserEntity } from '../user/User.Entity';
import { CandidateResponseModel } from '../candidate/Candidate.Entity';
import {
    ApplicationStepInputModel,
    ApplicationStepEntity,
    ApplicationStepUpdateModel,
} from '../application/Application.Step.Entity';
//Enums
import { ApplicationOrigin } from '@/Enums';
import ApplicationStatusEnum from '../../enums/Application.Status.Enum';

//Entity
export interface ApplicationEntity {
    _id: string | ObjectId;
    job_id: ObjectId | JobResponseModel;
    candidate_id: ObjectId | CandidateResponseModel;
    status: ApplicationStatusEnum;
    currentStep?: Number;
    steps?: ApplicationStepEntity[];
    feedbacks?: ApplicationFeedbackEntity;
    origin?: ApplicationOrigin;
    startAt?: Date;
    updatedAt?: Date;
    createdAt?: Date;
}

export interface ApplicationFeedbackEntity {
    feedback_reasons: string;
    feedback_strenghts?: string;
    feedback_improvements?: string;
    request_glassdoor: boolean;
    createdBy: UserEntity | UserEntity['_id'];
    createdAt?: Date;
    updatedAt?: Date;
}

//ResponseModel
export interface ApplicationResponseModel extends ApplicationEntity {
    job_id: JobResponseModel;
    candidate_id: CandidateResponseModel;
}

//ArgsModel
export interface ApplicationArgsModel {
    application_id?: string[];
    status?: ApplicationStatusEnum[];
}

//InputModel
export interface ApplicationInputModel {
    job_id: string;
    candidate_id: string;
    startAt?: Date;
    steps?: ApplicationStepInputModel[];
    origin?: ApplicationOrigin;
}

//Update
export interface ApplicationUpdateModel {
    status?: ApplicationStatusEnum;
    steps?: ApplicationStepUpdateModel[];
    feedbacks?: ApplicationFeedbackEntity;
    currentStep?: Number;
    startAt?: Date;
    origin?: ApplicationOrigin;
}
