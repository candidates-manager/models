import { ObjectId } from 'mongoose';
import { UserEntity } from '../user/User.Entity';
import { CompanyEntity } from '../company/Company.Entity';
export interface ApplicationProcessStepEntity {
    step: Number;
    name: String;
    description: String;
    duration?: Number;
    users?: UserEntity[] | ObjectId[];
}
export interface ApplicationProcessEntity {
    _id: string | ObjectId;
    company_id: CompanyEntity[] | ObjectId[];
    name: string;
    steps: ApplicationProcessStepEntity[];
    updatedAt?: Date;
    createdAt?: Date;
}

//Update
export interface ApplicationProcessStepUpdateModel {
    step?: Number;
    name?: String;
    description?: String;
    duration?: Number;
    users?: string[];
}
export interface ApplicationProcessUpdateModel {
    name?: string;
    steps?: ApplicationProcessStepUpdateModel[];
}

//Response
export interface ApplicationProcessResponseModel
    extends ApplicationProcessEntity {
    _id: string;
}
export interface ApplicationProcessStepInputModel {
    step: Number;
    name: String;
    description?: String;
    duration?: Number;
    users?: string[];
}

//Input
export interface ApplicationProcessInputModel {
    name: string;
    company_id: string[];
    steps: ApplicationProcessStepInputModel[];
}
export interface ApplicationProcessArgsModel {
    company_id: string[];
    public_id?: string;
}
