import { ObjectId } from 'mongoose';

//Enums
import { ReplyTypeEnum } from '@/Enums';

export interface ReplyEntity {
    _id: ObjectId;
    entity_id: ObjectId;
    entity_type: ReplyTypeEnum;
    user_id: ObjectId;
    content: string;
    updatedAt: Date;
    createdAt: Date;
    parent_id?: ObjectId;
}

export interface ReplyResponseModel extends ReplyEntity {}

export interface ReplyInputModel {
    entity_id: string;
    entity_type: ReplyTypeEnum;
    user_id: string;
    content: string;
    parent_id?: string;
}

export interface ReplyUpdateModel {
    content: string;
}

export interface ReplyArgsModel {
    entity_id: string;
    entity_type?: ReplyTypeEnum;
}
