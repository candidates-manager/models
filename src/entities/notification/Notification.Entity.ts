import { ObjectId } from 'mongoose';

//Entity
export interface NotificationEntity {
    _id: string | ObjectId;
}

//ResponseModel
export interface NotificationResponseModel extends NotificationEntity {}

//InputModel
export interface NotificationInputModel {}

//Update
export interface NotificationUpdateModel {}

//ArgsModel
export interface NotificationArgsModel {}
