enum ApplicationOrigin {
    linkedin = 'Linkedin',
    glassdoor = 'Glassdoor',
    website = "Company's website",
    cooptation = 'Cooptation',
    spontaneous = 'Spontaneous Application',
    others = 'others',
}
export default ApplicationOrigin;
