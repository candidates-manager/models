enum StatusEnum {
    active = 'Active',
    archived = 'Archived',
}
export default StatusEnum;
