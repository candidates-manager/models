enum UserStatusEnum {
    active = 'active',
    onboarding = 'OnBoarding',
    inactive = 'inactive',
    blocked = 'blocked',
}
export default UserStatusEnum;
