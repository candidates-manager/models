enum EntityEnum {
    application = 'Application',
    application_step = 'Step Application',
    job = 'Job',
}
export default EntityEnum;
