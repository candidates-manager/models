enum Role {
    admin = 'Admin',
    recruiter = 'Recruiter',
    employee = 'Employee',
    candidate = 'Candidate',
}
export default Role;
