enum Currency {
  eur = 'eur',
  gbp = 'gbp',
  usd = 'usd',
  jpg = 'jpg',
}
export default Currency;
