enum ReplyTypeEnum {
    application_team = 'Application Team',
    application_candidate = 'Application Candidate',
}
export default ReplyTypeEnum;
