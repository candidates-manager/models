enum ApplicationStatusEnum {
    inProcess = 'In Process',
    refused = 'Refused',
    hired = 'Hired',
}
export default ApplicationStatusEnum;
