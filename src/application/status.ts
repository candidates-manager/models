export interface StatusEntity {
    message: string;
    type: 'success' | 'error' | 'warn' | 'info';
}
