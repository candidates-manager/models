export interface MailEntity {
    subject: string;
    htmlContent: string;
    sendToEmail: string;
    sendToName: string;
    templateId?: number;
    sendCopyEmail?: string;
    sendCopyName?: string;
    params?: {
        callToAction?: string;
        login?: string;
        password?: string;
    };
}
