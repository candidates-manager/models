export interface ApiResponseModel {
    success: boolean;
    status: string;
    statusCode: Number;
    data: any;
    errors: any;
}
